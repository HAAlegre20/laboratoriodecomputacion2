package procesadordetexto;

import sun.management.jmxremote.SingleEntryRegistry;

import javax.swing.*;
import javax.swing.text.StyledEditorKit;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.*;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;




class procesadorDeTexto {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        FramePrincipal mimarco=new FramePrincipal();
        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

class FramePrincipal extends JFrame {
    private JButton jbGuardar;
    private JScrollPane jspTexto;

    public FramePrincipal(){
        setBounds(500,200,550,420);
        setTitle("*Text*");
        setResizable(false);
        PanelPrincipal panel=new PanelPrincipal(this);
      
        setVisible(true);
        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        add(scrollPane);

    }
}

class PanelPrincipal extends JPanel{
    Archivo oArchivo;
    JTextPane areaDeTexto;
    JMenu Archivo;
    JMenu Fuente;
    JMenu Estilo;
    JMenu Tamaño;
    JFrame  parentFrame;

    public PanelPrincipal(JFrame frame){
        parentFrame = frame;
        oArchivo = new Archivo(frame);
        areaDeTexto =new JTextPane();

        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                oArchivo.autoguardado(areaDeTexto.getText());

                super.windowClosing(e);
            }

        });


        
        setLayout(new BorderLayout());
        JPanel laminamenu=new JPanel();
        JMenuBar mibarra=new JMenuBar();
        //---------------------------------------------------------

        Archivo=new JMenu("Archivo");
        Fuente=new JMenu("Fuente");
        Estilo=new JMenu("Estilo");
        Tamaño =new JMenu("Tamaño");

        configura_menu("Arial","Fuente","Arial",9,10);
        configura_menu("Courier","Fuente","Courier",9,10);
        configura_menu("Verdana","Fuente","Verdana",9,10);
        //--------------------------------------------------------
        configura_menu("Negrita","Estilo","",Font.BOLD,1);
        configura_menu("Cursiva","Estilo","",Font.ITALIC,1);
        //---------------------------------------------------------
        configura_menu("Guardar", "Archivo","",Font.BOLD,1);
        configura_menu("Abrir", "Archivo","",Font.ITALIC,2);
        //---------------------------------------------------------
        ButtonGroup Tamaño_letra=new ButtonGroup();
        JRadioButtonMenuItem doce=new JRadioButtonMenuItem("12");
        JRadioButtonMenuItem dieciseis=new JRadioButtonMenuItem("16");
        JRadioButtonMenuItem veinte=new JRadioButtonMenuItem("20");
        JRadioButtonMenuItem veinticuatro=new JRadioButtonMenuItem("24");

        Tamaño_letra.add(doce);
        Tamaño_letra.add(dieciseis);
        Tamaño_letra.add(veinte);
        Tamaño_letra.add(veinticuatro);
        doce.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamaño", 12));
        dieciseis.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamaño", 16));
        veinte.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamaño", 20));
        veinticuatro.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamaño", 24));
        Tamaño.add(doce);
        Tamaño.add(dieciseis);
        Tamaño.add(veinte);
        Tamaño.add(veinticuatro);
        //-----------------------------------------------------------
        mibarra.add(Archivo);
        mibarra.add(Fuente);
        mibarra.add(Estilo);
        mibarra.add(Tamaño);

        laminamenu.add(mibarra);
        add(laminamenu,BorderLayout.NORTH);


        add(areaDeTexto,BorderLayout.CENTER);
        JPopupMenu menuEmergente=new JPopupMenu();
        JMenuItem negritaE=new JMenuItem("Negrita");
        JMenuItem cursivaE=new JMenuItem("Cursiva");
        negritaE.addActionListener(new StyledEditorKit.BoldAction());
        cursivaE.addActionListener(new StyledEditorKit.ItalicAction());
        menuEmergente.add(negritaE);
        menuEmergente.add(cursivaE);
        areaDeTexto.setComponentPopupMenu(menuEmergente);
        //----------------------------------------------------------
        JMenuItem guardarE=new JMenuItem("Guardar");
        JMenuItem abrirE=new JMenuItem("Abrir");


        guardarE.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                oArchivo.escribir_archivo(areaDeTexto.getText());
            }
        });

        abrirE.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                areaDeTexto.setText(oArchivo.leer_archivo());
            }
        });

        Archivo.add(guardarE);
        Archivo.add(abrirE);

    }

    public void configura_menu(String textoMenu, String menu, String tipo_letra, int estilos, int tamañoLetra){

        JMenuItem elem_menu=new JMenuItem(textoMenu);

        if(menu=="Fuente"){
            Fuente.add(elem_menu);
            if(tipo_letra=="Arial"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Arial"));
            }else if(tipo_letra=="Courier"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Courier"));
            }else if(tipo_letra=="Verdana"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Verdana"));
            }
        }
        else if(menu=="Estilo"){
            Estilo.add(elem_menu);
            if(estilos==Font.BOLD){
                elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
                elem_menu.addActionListener(new StyledEditorKit.BoldAction());
            }else if(estilos==Font.ITALIC){
                elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K,InputEvent.CTRL_DOWN_MASK));
                elem_menu.addActionListener(new StyledEditorKit.ItalicAction());
            }
        }
        else if(menu=="Tamaño"){
            Tamaño.add(elem_menu);
            elem_menu.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamaño", tamañoLetra));
        }
    }
}

class Archivo{
    private File flArchivo;
    private String Ruta_del_archivo;
    private BufferedWriter bwEscritor;
    private FileWriter fwArchivo_en_el_que_escribir;
    private FileDialog fdGuardar;
    private FileDialog fdAbrir;

    
    public Archivo(JFrame frame)
    {
        fdGuardar=new FileDialog(frame, "Guardar como", FileDialog.SAVE);
        fdAbrir=new FileDialog(frame, "Abrir", FileDialog.LOAD);
        Ruta_del_archivo="";
        flArchivo=new File(Ruta_del_archivo);
    }

    public String escribir_archivo(String texto_a_guardar)
    {
        String mTextos[]=texto_a_guardar.split("\n"), respuesta="";//Se detecta el salto de linea, se va colocando en el vector.

        fdGuardar.setVisible(true);
        fdGuardar.dispose();
        Ruta_del_archivo=fdGuardar.getDirectory()+fdGuardar.getFile() + ".txt";
        flArchivo=new File(Ruta_del_archivo);

       
        if(flArchivo.exists()){
            respuesta="El archivo ya existia y ha sido sobreescrito. \nEn la direccion: " +  Ruta_del_archivo ;
        }
        else{
            respuesta="El archivo ha sido creado exitosamente.\nEn la direccion: " +  Ruta_del_archivo ;
        }

        try{
            fwArchivo_en_el_que_escribir=new FileWriter(flArchivo);//De momento se deja hasta aqui, listo para escribir, se escribe en el momento de dar la orden.
            for (int i=0; i<=mTextos.length-1;i++)
            {
                fwArchivo_en_el_que_escribir.write(mTextos[i] + "\r\n" ); //Se va escribiendo cada linea en el archivo de texto.
            }

            bwEscritor=new BufferedWriter(fwArchivo_en_el_que_escribir); //Este metodo escribe el archivo en el disco duro.
            bwEscritor.close();//Se cierra el archivo.
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(null,ex.getMessage());
        }

        return respuesta;
    }

    public String leer_archivo()
    {
        fdAbrir.setVisible(true);
        fdAbrir.dispose();
        Ruta_del_archivo=fdAbrir.getDirectory()+fdAbrir.getFile();
        flArchivo=new File(Ruta_del_archivo);

        String contenido = "";

        try  {
            FileReader fr = new FileReader(flArchivo);
            int value = fr.read();
            while (value != -1) {
                contenido = contenido + (char) value;
                value = fr.read();
            }

        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return contenido;
    }

    public String autoguardado(String texto_a_guardar)
    {
        String mTextos[]=texto_a_guardar.split("\n"), respuesta="";

        flArchivo=new File("autoguardado.txt");

        try{
            fwArchivo_en_el_que_escribir=new FileWriter(flArchivo);
            for (int i=0; i<=mTextos.length-1;i++)
            {
                fwArchivo_en_el_que_escribir.write(mTextos[i] + "\r\n" ); 
            }

            bwEscritor=new BufferedWriter(fwArchivo_en_el_que_escribir); 
            bwEscritor.close();
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(null,ex.getMessage());
        }
        JOptionPane.showMessageDialog(null,"El archivo se guardo en " + flArchivo.getAbsolutePath());
        return respuesta;
    }


}


   
        
        
        
        
 