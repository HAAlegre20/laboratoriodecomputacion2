
public class Desafio07 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

            //FUNCIONES TRIGONOMETRICAS		
		    double angulo = 45.0 * Math.PI/180.0;
		    System.out.println("cos(" + angulo + ") es " + Math.cos(angulo));
		    System.out.println("sin(" + angulo + ") es " + Math.sin(angulo));
		    System.out.println("tan(" + angulo + ") es " + Math.tan(angulo));
		
		    double y=-6.2;  //ordenada
		    double x=1.2;   //abscisa
		    System.out.println("atan2(" + y+" , "+x + ") es " + Math.atan2(y, x));
		
		
		
		    //FUNCION EXPONENCIAL
		    System.out.println("exp(1.0) es " +  Math.exp(1.0));
		    System.out.println("exp(10.0) es " + Math.exp(10.0));
		    System.out.println("exp(0.0) es " +  Math.exp(0.0));

		    //FUNCION INVERSA
		    System.out.println("log(1.0) es " + Math.log(1.0));
		    System.out.println("log(10.0) es " + Math.log(10.0));
		    System.out.println("log(Math.E) es " + Math.log(Math.E));
		
		    
		    
		   //COSTANTES PI e
		    final class Math {
		        public static final double E = 2.7182818284590452354;
		        public static final double PI = 3.14159265358979323846;
		        
		    }
		  
		         System.out.println("Pi es " + Math.PI);     
		         System.out.println("e es " + Math.E);   
		
	}

}
